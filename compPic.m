%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% --------------------------------------------------------------- %%
%%                         P A R T - I                             %%
%%        R E S E T I N G   T H E   E N V I R O N E M E N T        %%
%% --------------------------------------------------------------- %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function compPic
% you must keep definition of many following functions depends on it


%% the following code is for compressing jpg images, the following
%% code is written by Mohamed Mansour Mohamed ElRayany as a part
%% of the final project of ([ECE 451] Digital Signal Processing 1),  
%% under the supervision of Dr. Bassant Abdelhamid in December 2015.
%%
%% to initialize the code, insert any jpg image of any size and color.  
%% The code will covert it into a grayscale image then resize it into  
%% 128x128, then perform DCT transform on it to reduce its size. it  
%% is preferable to insert the picture attached with the code to get 
%% the optimal results. thank you.

% Reset Matlab environment
clear; clc; close all; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% --------------------------------------------------------------- %%
%%                            P A R T - II                         %%
%%                R E S I Z I N G   T H E   I M A G E              %%
%%          P E R F O R M   2D   D C T   T R A N S F O R M         %%
%% --------------------------------------------------------------- %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% for the input picture, perform a 2 dimensional dct on 8x8 blocks.
%% save the dct inside a cell of the size: 10 cells of 128x128 matrix
%% show for each picture, it's dct 8x8 block transform.

% load the image
rgb_img = imread( 'input.jpg' ); 

% display the RGB image
subplot(2,2,1);
imshow( rgb_img ) 
axis image; title( sprintf ( 'Original Image' ) );

% convert the image into grayscale image
grayscale_img = .2989*rgb_img( :,:,1 )...
	+.5870*rgb_img( :,:,2 )...
	+.1140*rgb_img( :,:,3 );
colormap( gray( 256 ) ); 

% Display the grayscale image
subplot(2,2,2);
imshow( grayscale_img ); 
axis image; title( sprintf( 'The Grayscale Image' ) );

% resize the a picture. im2double(grayscale_img) converts the grayscale  
% intensity image "grayscale_img" to double precision and rescaling the 
% data if necessary
input_image_128x128 = im2double( imresize( grayscale_img , [128, 128] ) );

% perform DCT in 2 dimension over blocks of 8x8 in the given picture
dct_8x8_image_of_128x128 = image_8x8_block_dct( input_image_128x128 );

% Display the resized grayscale image
subplot(2,2,3);
imshow( input_image_128x128 );
title( sprintf('Resized Grayscale Image') );
% Display the image after the DCT
subplot(2,2,4);
imshow( dct_8x8_image_of_128x128 );
title( sprintf('Input Image After Taking DCT Transform') );

% add a title to the figure	
axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible',...
    'off','Units','normalized','clipping' , 'off');
text(0.5, 1,'\bf Showing the RGB & Grayscale Images','HorizontalAlignment',...
    'center','VerticalAlignment','top','FontSize',12);	
axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1.85],'Box','off','Visible',...
    'off','Units','normalized','clipping' , 'off');
text(0.5, 1,'\bf Showing the Resized Grayscale and After Taking DCT Transform',...
    'HorizontalAlignment', 'center','VerticalAlignment','top','FontSize',12);
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% --------------------------------------------------------------- %%
%%                        P A R T - III                            %%
%%         C A L C L A T E   T H E   M E A N   M A T R I X         %%
%%          F I N D   T H E   C O E F I C I E N T S   O F          %%
%%                H I G H E S T   V A R I A N C E                  %%
%% --------------------------------------------------------------- %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% do statistics on the cell array of the DCT transforms. create 
%% a matrix of 8x8 that will describe the value of each "dct-base" 
%% over the transform of the given picture. since some of the values
%% are negative, and we are interested in the variance of the coeffs,
%% I will add the abs()^2 values into the matrix. This is consistent
%% with the definition of the "Parseval relation" in Fourier coeffs

% initialize the "average" matrix 
mean_matrix_8x8 = zeros( 8,8 );

% in the input picture, loop over 8x8 elements (128x128 = 256 * 8x8 elements)
for m = 0:15
  for n = 0:15
	 mean_matrix_8x8 = mean_matrix_8x8 + ...
	 abs(dct_8x8_image_of_128x128(m*8+[1:8],n*8+[1:8]) ).^2;
  end
end

% transpose the matrix since the order of the matrix is elements along the 
% columns, while in the subplot function the order is of elements along the rows
mean_matrix_8x8_transposed = mean_matrix_8x8';

% make the mean matrix (8x8) into a vector (64x1)
mean_vector = mean_matrix_8x8_transposed(:);

% sort the vector (from small to big)
[sorted_mean_vector,original_indices] = sort( mean_vector );

% reverse order (from big to small)
sorted_mean_vector = sorted_mean_vector(end:-1:1);
original_indices = original_indices(end:-1:1);

% plot the corresponding matrix
figure;
for idx = 1:64
   subplot(8,8,original_indices(idx));
   axis off;
   h = text(0,0,sprintf('%4d',idx));
   set(h,'FontWeight','bold');
   text(0,0,sprintf(' \n_{%1.1fdb} ',20*log10(sorted_mean_vector(idx)) ));
end

% add a title to the figure
axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible',...
    'off','Units','normalized','clipping' , 'off');
text(0.5, 1,'\bf Power of DCT Coefficients','HorizontalAlignment',...
    'center','VerticalAlignment','top','FontSize',12);
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% --------------------------------------------------------------- %%
%%                          P A R T - IV                           %%
%%      E X P E R I M E N E T   O N   T H E   N U M B E R   O F    %%
%%              T H E   C O E F F I C E N T S   U S E D            %%
%%          P E R F O R M   T H E   Q U A N T I Z A T I O N        %%
%%                 R E S T O R   T H E   I M A G E                 %%
%% --------------------------------------------------------------- %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% find the coefficients that have the maximum variance, and change the 
%% number of coefficient used in the restoration of the original image. 
%% For instance, variance, if I decide to take only 20 coefficients with  
%% the highest variance. I will add zeros to the rest of the coefficients   
%% and remain with a vector 64 elements long (or a matrix of 8x8). I    
%% will use this matrix to choose only the wanted number of coefficients. 
%% This method is based on the zonal coding that is explained in Rafael  
%% Gonzalez's book "Digital Image Processing"

% the matrix is initialized to zeros
coef_selection_matrix = zeros(8,8);

% compressed picture set (to show the degrading)
compressed_set = [1 3 6 10 15 25 45 64];

% this loop will choose each time, the coefficients of the highest variance, 
% to be added to the compressed image -> and thus to improve the RMS
for number_of_coefficient = 1:64
    
    % find the coefficients of the highest variance, from the mean_matrix
    [y,x] = find(mean_matrix_8x8==max(max(mean_matrix_8x8)));
    
    % select the coefficients for the compressed image
    coef_selection_matrix(y,x) = 1;
    
    % replicate the selection matrix for all the parts of the DCT transform
    % DCT transform creates a set of 8x8 matrices, where in each matrix I 
	% need to choose the coefficients defined by the 
    % <<coef_selection_matrix>> matrix
    selection_matrix = repmat( coef_selection_matrix,16,16 );
    
    % set it as zero in the mean_matrix, so that in the next loop, I will
    % choose the coefficients of the highest variance 
    mean_matrix_8x8(y,x) = 0;
    
    % choose the coefficients of the highest variance from the resized grayscale image
    % (total of <<number_of_coefficient>> coefficients for this run in the loop)
    compressed_image = image_8x8_block_dct(input_image_128x128) .* selection_matrix;
    
    % restore the compressed image from the given set of coefficients
    restored_image = image_8x8_block_inv_dct( compressed_image );
    
	% Write the image after compressing it
	imwrite(restored_image,'output.jpg')
	    
	% show the image with different restoration coefficients
    if ~isempty(find(number_of_coefficient==compressed_set))
        if (number_of_coefficient==1)
            figure;
            subplot(3,3,1);
            imshow( input_image_128x128 );
            title( 'Resized Grayscale Image' );
        end
        subplot(3,3,find(number_of_coefficient==compressed_set)+1);
        imshow( restored_image );
        title( sprintf('Restored Image With %d Coeffs',number_of_coefficient) );
    end
end

% add a title to the figure
axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off',...
 'Units','normalized','clipping' , 'off');
text(0.5, 1,'\bf Effect of Increasing the Coefficients','HorizontalAlignment',...
 'center','VerticalAlignment','top','FontSize',12);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% --------------------------------------------------------------- %%
%%                          P A R T - V                            %%
%%          F U N C T I O N   I M P L E M E N T A T I O N          %%
%% --------------------------------------------------------------- %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% --------------------------------------------------------------- %%
%%      image_8x8_block_dct - perform a block DCT for an image     %%
%% --------------------------------------------------------------- %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function transform_image = image_8x8_block_dct( input_image ) 

transform_image = zeros( size( input_image,1 ),size( input_image,2 ) );
for m = 0:15
    for n = 0:15
        transform_image( m*8+[1:8],n*8+[1:8] ) = ...
            pdip_dct2( input_image( m*8+[1:8],n*8+[1:8] ) );
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% --------------------------------------------------------------- %%
%%       pdip_dct2 - implementation of a 2 Dimensional DCT         %%
%% --------------------------------------------------------------- %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function out = pdip_dct2( in ) 

% Preallocate matrix C for speeding up calculations
C=zeros(8,8);

% get input matrix size
N = size(in,1);

% build the matrix of the DCT transform
n = 0:N-1;
for k = 0:N-1
   if (k>0)
      C(k+1,n+1) = cos(pi*(2*n+1)*k/2/N)/sqrt(N)*sqrt(2);
   else
      C(k+1,n+1) = cos(pi*(2*n+1)*k/2/N)/sqrt(N);
   end   
end

out = C*in*(C');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ------------------------------------------------------------------ %%
%% image_8x8_block_inv_dct - perform a block inverse DCT for an image %%
%% ------------------------------------------------------------------ %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function restored_image = image_8x8_block_inv_dct( transform_image )

restored_image = zeros( size( transform_image,1 ), size( transform_image,2 ) );
for m = 0:15
    for n = 0:15
        restored_image( m*8+[1:8],n*8+[1:8] ) = ...
            pdip_inv_dct2( transform_image( m*8+[1:8],n*8+[1:8] ) );
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% --------------------------------------------------------------- %%
%%  pdip_inv_dct2 - implementation of an inverse 2 Dimensional DCT %%
%% --------------------------------------------------------------- %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function out = pdip_inv_dct2( in ) 

% Reallocate matrix C for speeding up calculations
C=zeros(8,8);

% get input matrix size
N = size(in,1);

% build the matrix of the inverse DCT transform
n = 0:N-1;
for k = 0:N-1
   if (k>0)
      C(k+1,n+1) = cos(pi*(2*n+1)*k/2/N)/sqrt(N)*sqrt(2);
   else
      C(k+1,n+1) = cos(pi*(2*n+1)*k/2/N)/sqrt(N);
   end   
end

out = (C')*in*C;
